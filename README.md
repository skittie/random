# random

Random bits and snippets of code. Every single file here would need a clean-up if I wasn't so lazy.

## Skynet

Wrapper for the cobe chatbot library, enabling better IRC-bot-functionality.

### Requirements
1. Cobe
2. Twisted

## Massquery

Experimental IRC spambot using SOCKS, written somewhere around 2013. 
For educational purposes only.

### Requirements
1. PySocks